# API Proxy

An NGINX based proxy, intended to handle static assets in front of a REST API.

## Usage

### Environment variables

- `LISTEN_PORT` - The port on which the proxy listens (default: `8000`)
- `APP_HOST` - The hostname of the underlying app to which the proxy forwards requests (default: `localhost`)
- `APP_PORT` - The port on which the underlying app is running (default: `9000`)
