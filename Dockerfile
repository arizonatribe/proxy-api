FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="arizonatribe@gmail.com"

ENV APP_HOST=app
ENV APP_PORT=9000
ENV LISTEN_PORT=8000

USER root

# Overlay the container's linux file system
# with the local files & folders
# (preserving directory structure)
COPY root /

# Despite being owned by root, ensure they can be read and executed by everyone else
RUN chmod 755 /var/lib/assets

# The entrypoint script will use the default.conf.tpl
# to create the default.conf new, on each startup.
# This ensures the nginx user will still own
# the version of that file that is re-generated over top of it.
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# Switch over to a less-privileged user
USER nginx

# Source the env vars into the .tpl file
# to create the nginx default.conf file
# prior to starting the NGINX daemon
CMD ["/usr/bin/start"]
